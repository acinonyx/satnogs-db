---
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: 'Jobs/Container-Scanning.gitlab-ci.yml'
  - template: 'Jobs/Dependency-Scanning.gitlab-ci.yml'
  - template: 'Jobs/SAST.gitlab-ci.yml'
  - template: 'Jobs/Secret-Detection.gitlab-ci.yml'
variables:
  GITLAB_CI_IMAGE_ALPINE: 'alpine:3.11'
  GITLAB_CI_IMAGE_DOCKER: 'docker:20.10.24'
  GITLAB_CI_IMAGE_NODE: 'node:16.16'
  GITLAB_CI_IMAGE_PYTHON: 'python:3.11.9'
  GITLAB_CI_IMAGE_OPENAPI_GENERATOR_CLI: 'openapitools/openapi-generator-cli:v5.3.0'
  GITLAB_CI_IMAGE_SENTRY_CLI: 'getsentry/sentry-cli'
  GITLAB_CI_PYPI_DOCKER_COMPOSE: 'docker-compose~=1.23.0'
  GITLAB_CI_SIGN_OFF_EXCLUDE: 'a94d14ba71ff481e7e27bdd4ed549424e0963322'

stages:
  - schema
  - api
  - static
  - build
  - test
  - deploy
  - sentry_release
  - trigger
  - security

# 'schema' stage
schema:
  stage: schema
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  script:
    - pip install --no-cache-dir --no-deps -r "requirements.txt" --force-reinstall .
    - >-
      ./manage.py spectacular
      --file satnogs-db-api-client/api-schema.yml
      --validate
      --fail-on-warn
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - satnogs-db-api-client

# 'api' stage
api:
  stage: api
  needs:
    - job: schema
      artifacts: true
  image: ${GITLAB_CI_IMAGE_OPENAPI_GENERATOR_CLI}
  script:
    - >-
      docker-entrypoint.sh
      generate
      -i satnogs-db-api-client/api-schema.yml
      -g python
      -o satnogs-db-api-client
      -c satnogs-db-api-client/openapi-generator-config.json
    - >-
      docker-entrypoint.sh
      generate
      -i satnogs-db-api-client/api-schema.yml
      -g html2
      -o satnogs-db-api-client/html2
      -c satnogs-db-api-client/openapi-generator-config.json
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - satnogs-db-api-client

# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: >-
    git log
    --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$"
    --invert-grep
    --format="Detected commit '%h' with missing or bad sign-off! Please read 'CONTRIBUTING.md'."
    --exit-code
    $(rev=$(git rev-parse -q --verify "$GITLAB_CI_SIGN_OFF_EXCLUDE^{commit}") && echo "$rev..")
static_js_css:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_NODE}
  script:
    - npm ci
    - node_modules/.bin/gulp
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - db/static/lib
static:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - tox run -e "flake8,isort,yapf,pylint"

# 'build' stage
docs:
  stage: build
  needs: []
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - rm -rf docs/_build
    - tox run -e "docs"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - docs/_build/html
build:
  stage: build
  needs:
    - job: static_js_css
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - rm -rf dist
    - tox run -e "build"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - dist
build_api:
  stage: build
  needs:
    - job: api
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -csatnogs-db-api-client/constraints.txt tox
  script:
    - cd satnogs-db-api-client
    - rm -rf dist
    - tox run -e "build"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - satnogs-db-api-client/dist

# 'test' stage
test:
  stage: test
  needs:
    - job: static_js_css
      artifacts: true
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - tox run-parallel -e "deps,pytest"

# 'deploy' stage
docker:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DOCKER}
  services:
    - ${GITLAB_CI_IMAGE_DOCKER}-dind
  script:
    - |
      [ -z "$CI_REGISTRY_IMAGE" ] || {
          CACHE_IMAGE="$CI_REGISTRY_IMAGE/satnogs-db:$CI_COMMIT_REF_NAME"
          [ -z "$CI_COMMIT_TAG" ] || CACHE_IMAGE="$CI_REGISTRY_IMAGE/satnogs-db:latest"
          export CACHE_IMAGE
      }
    - docker-compose -f docker-compose.yml -f docker-compose.cache.yml pull cache_image || true
    - docker-compose -f docker-compose.yml -f docker-compose.cache.yml build --pull
    - |
      [ -z "$CI_REGISTRY_IMAGE" ] || {
          echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
          docker tag satnogs-db:latest $CI_REGISTRY_IMAGE/satnogs-db:$CI_COMMIT_REF_NAME
          docker push $CI_REGISTRY_IMAGE/satnogs-db:$CI_COMMIT_REF_NAME
          [ -z "$CI_COMMIT_TAG" ] || {
              docker tag satnogs-db:latest $CI_REGISTRY_IMAGE/satnogs-db:latest
              docker push $CI_REGISTRY_IMAGE/satnogs-db:latest
          }
      }
      [ -z "$DOCKERHUB_PASSWORD" ] || {
          echo "$DOCKERHUB_PASSWORD" | docker login -u $DOCKERHUB_USER --password-stdin
          docker tag satnogs-db:latest librespace/satnogs-db:$CI_COMMIT_REF_NAME
          docker push librespace/satnogs-db:$CI_COMMIT_REF_NAME
          [ -z "$CI_COMMIT_TAG" ] || {
              docker tag satnogs-db:latest librespace/satnogs-db:latest
              docker push librespace/satnogs-db:latest
          }
      }
  only:
    refs:
      - master
      - tags
deploy:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -cconstraints.txt tox
  script:
    - rm -rf dist
    - tox run -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD
  except:
    - triggers
deploy_api:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_PYTHON}
  before_script:
    - pip install -csatnogs-db-api-client/constraints.txt tox
  script:
    - cd satnogs-db-api-client
    - rm -rf dist
    - tox run -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD
  except:
    - triggers
pages:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_ALPINE}
  script:
    - mv docs/_build/html/ public/
    - mv satnogs-db-api-client/html2/ public/api/
  artifacts:
    paths:
      - public
  only:
    - tags

# 'sentry_release' stage
sentry_release:
  stage: sentry_release
  image: ${GITLAB_CI_IMAGE_SENTRY_CLI}
  script:
    - sentry-cli releases new --finalize -p ${CI_PROJECT_NAME} ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
    - sentry-cli releases set-commits --auto ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
  only:
    refs:
      - tags
    variables:
      - $SENTRY_AUTH_TOKEN
      - $SENTRY_ORG

# 'trigger' stage
trigger_master:
  stage: trigger
  needs:
    - job: docker
      artifacts: false
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache curl
  script:
    - PIPELINE_TRIGGERS_MASTER=$(echo "$PIPELINE_TRIGGERS_MASTER" | sed 's/{{CI_COMMIT_SHORT_SHA}}/'"$CI_COMMIT_SHORT_SHA"'/g')
    - for trigger in $PIPELINE_TRIGGERS_MASTER; do curl -X POST "$trigger"; done
  only:
    refs:
      - master
    variables:
      - $PIPELINE_TRIGGERS_MASTER
trigger_latest:
  stage: trigger
  needs:
    - job: docker
      artifacts: false
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache curl
  script:
    - PIPELINE_TRIGGERS_LATEST=$(echo "$PIPELINE_TRIGGERS_LATEST" | sed 's/{{CI_COMMIT_TAG}}/'"$CI_COMMIT_TAG"'/g')
    - for trigger in $PIPELINE_TRIGGERS_LATEST; do curl -X POST "$trigger"; done
  only:
    refs:
      - tags
    variables:
      - $PIPELINE_TRIGGERS_LATEST

# 'security' stage
container_scanning:
  stage: security
  needs:
    - job: docker
      artifacts: false
  variables:
    CI_APPLICATION_REPOSITORY: ${CI_REGISTRY_IMAGE}/satnogs-db
    CI_APPLICATION_TAG: ${CI_COMMIT_REF_NAME}
  rules:
    - if: $CI_REGISTRY_IMAGE && $CI_COMMIT_BRANCH == "master"
    - if: $CI_REGISTRY_IMAGE && $CI_COMMIT_TAG
dependency_scanning:
  stage: security
  variables:
    DS_EXCLUDED_ANALYZERS: 'gemnasium-maven,gemnasium-python'
sast:
  stage: security
  needs:
    - job: api
      artifacts: true
  variables:
    SAST_DISABLE_BABEL: 'true'
secret_detection:
  stage: security
  needs:
    - job: api
      artifacts: true
