/* global prepareTransmitterForm */

function validateFormAndSubmit(formURL, container, extraFormSetup) {
    /* This function works with  BSModal*View views. Those views will only validate and 
     * not save an object when they receive an XHR Post request. So if the ajax submitted
     * form is validated and has no errors, it is submitted normally (without xhr)
     */
    const form = container.find('form');
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: new FormData(form[0]),
        contentType: false,
        processData: false,
        beforeSend: function () {
            form.find('button[type="submit"]').prop('disabled', true);
        },
        success: function (response) {
            if ($(response).find('.invalid').length > 0) {  // If it has errors, re-render the form
                formSetup(response, formURL, container, extraFormSetup);
            } else {
                form.submit();  // If not, submit it without xhr
            }
        }
    });
}

function formSetup(response, formURL, container, extraFormSetup) {
    container.html(response);
    const form = container.find('form');
    form.attr('action', formURL);
    form.on('submit', function (event) {
        if (event.originalEvent !== undefined) {    // Prevent loop at submission
            event.preventDefault();
            validateFormAndSubmit(formURL, container, extraFormSetup);
            return false;
        }
    });
    if(extraFormSetup && typeof extraFormSetup === 'function') {
        extraFormSetup();
    }
}

function loadForm(formURL, extraFormSetup) {
    const container = $('#form-container');
    $.ajax({
        type: 'GET',
        url: formURL,
        success: function (response) {
            formSetup(response, formURL, container, extraFormSetup);
        }
    });
}

$(document).ready(function() {
    'use strict';

    const formContainer = document.getElementById('form-container');
    const suggestionType = formContainer.dataset['suggestionType'];
    const suggestionId = formContainer.dataset['suggestionId'];

    const FORM_URL = suggestionType === 'satellite' ? `/update_satellite/${suggestionId}?modal=False`: `/update_transmitter/${suggestionId}?modal=False`;

    loadForm(FORM_URL, suggestionType === 'transmitter' ? prepareTransmitterForm: undefined);
});